from django.test import TestCase,Client
from django.urls import resolve
from .views import home

class tester(TestCase):

    def test_status_code_is_ok(self):
        response=Client().get("/")
        self.assertEqual(response.status_code,200)

    def test_template_used(self):
        response=Client().get("/")
        self.assertTemplateUsed(response,"home.html")
    
    def test_function_used(self):
        response=resolve("/")
        self.assertEqual(response.func,home)
